const express = require('express');
const nanoid = require('nanoid');

const createRouter = connection =>{
    const router = express.Router();

    router.get('/',(req,res)=>{
        connection.query('SELECT * FROM `categories`',(error,results,fields)=>{
            if (error){res.status(500).send({error:'db err'});}
            res.send();
        })
    });

    router.post('/',(req,res)=>{
        const category = req.body;
        category.id = nanoid();
        if (req.file){
            category.image =req.file.filename;
        }
        connection.query('INSERT INTO `categories` (`title`,`description`) VALUES (?,?)',
            [category.title,category.description],
            (error,results)=>{
                if (error){
                    res.status(500).send({error:'db err'});
                }
                res.send({message:'ok'})
            });
        res.send({message: 'ok'});
    });
    return router;
};
module.exports = createRouter;
