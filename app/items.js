const express = require('express');
const nanoid = require('nanoid');
const multer = require('multer');
const path = require('path');
const config = require('../config');

const storage = multer.diskStorage({
    destination: function (req,file,cb) {
        cb(null,config.uploadPath)
    },
    filename: function (req,file,cb) {
        cb(null,nanoid()+path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = connection =>{
 const router = express.Router();

 router.get('/',(req,res)=>{
        connection.query('SELECT * FROM `items`',(error,results)=>{
            if (error){res.status(500).send({error:'db err'});}
            res.send(results);
        })
    });

 router.get('/:id',(req,res)=>{
     connection.query('SELECT * FROM `items` WHERE `id`=?',req.params.id,(error,results)=>{
         if (error){res.status(500).send({error:'db err'});}
         if (results[0]){
             res.send(results[0]);
         } else {res.status(404).send({error: 'Item not found'})
         }
     });
 });

 router.post('/',upload.single('image'),(req,res)=>{
     const item = req.body;
     item.id = nanoid();
     if (req.file){
         item.image =req.file.filename;
     }
     connection.query('INSERT INTO `items` (`title`,`price`,`description`,`image`) VALUES (?,?,?,?)',
         [item.title,item.price,item.description,item.image],
         (error,results)=>{
         if (error){res.status(500).send({error:'db err'});
         }
             res.send({message:'ok'})
         });
 });
 return router;
};
module.exports = createRouter;