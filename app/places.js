const express = require('express');
const nanoid = require('nanoid');

const createRouter = connection =>{
    const router = express.Router();

    router.get('/',(req,res)=>{
        connection.query('SELECT * FROM `places`',(error,results,fields)=>{
            if (error){res.status(500).send({error:'db err'});}
            res.send();
        })
    });
    router.post('/',(req,res)=>{
        const place = req.body;
        place.id = nanoid();
        if (req.file){
            place.image =req.file.filename;
        }
        connection.query('INSERT INTO `places` (`title`,`description`) VALUES (?,?)',
            [place.title,place.description],
            (error,results)=>{
                if (error){res.status(500).send({error:'db err'});
                }
                res.send({message:'ok'})
            });
        res.send({message: 'ok'});
    });
    return router;
};

module.exports = createRouter;