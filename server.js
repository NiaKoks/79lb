const express = require ('express');
const cors = require('cors');
const mysql = require('mysql');
// const db = require('./fileDb');
const items = require('./app/items');
const categories = require('./app/categories');
const places = require('./app/places');

// db.init();

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'user',
    password: '15011996',
    // database: ''
});

const port = 8000;

app.use('/items', items(connection));
app.use('/categories', categories(connection));
app.use('/places', places(connection));

connection.connect(function (err) {
    if (err){
        console.error('error connecting:' + err.stack);
        return;
    }
    console.log('connected as id '+ connection.threadId);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});

